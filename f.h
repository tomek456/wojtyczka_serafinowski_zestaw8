#ifndef f.h
#define f.h

void wypisywanieLP(float *tablicaLP, int wielkoscLP);
void wypisywanieX(float *tablicaX, int wielkoscX);
void wypisywanieY(float *tablicaY, int wielkoscY);
void wypisywanieRHO(float *tablicaRHO, int wielkoscRHO);

float sredniaX(float *tablicaX, int wielkoscX);
float sredniaY(float *tablicaY, int wielkoscY);
float sredniaRHO(float *tablicaRHO, int wielkoscRHO);

float odchylenieX(float *tablicaX, int wielkoscX);
float odchylenieY(float *tablicaY, int wielkoscY);
float odchylenieRHO(float *tablicaRHO, int wielkoscRHO);

void sortowanieX(float *tablicaX, int wielkoscX);
void sortowanieY(float *tablicaY, int wielkoscY);
void sortowanieRHO(float *tablicaRHO, int wielkoscRHO);

float medianaX(float *tablicaX, int wielkoscX);
float medianaY(float *tablicaY, int wielkoscY);
float medianaRHO(float *tablicaRHO, int wielkoscRHO);

#endif
