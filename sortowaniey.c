#include "f.h"

void sortowanieY(float *tablicaY, int wielkoscY);

void sortowanieY(float *tablicaY, int wielkoscY)
{
    int i, j, k;
	for (i=0; i<wielkoscY-1; i++)
	{
		for (j=0; j<wielkoscY-1-i; j++)
		{
			if (tablicaY[j]>tablicaY[j+1])
			{
				k=tablicaY[j+1];
				tablicaY[j+1]=tablicaY[j];
				tablicaY[j]=k;
			}
		}
    }
    printf("Tablica Y z posortowanymi liczbami: \n");
    wypisywanieY(tablicaY, wielkoscY);
    printf("\n");
    return;
}
