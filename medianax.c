#include "f.h"

float medianaX(float *tablicaX, int wielkoscX);

float medianaX(float *tablicaX, int wielkoscX)
{
    float medianaX=0.0;
    if (wielkoscX%2==0)
    {
        medianaX=tablicaX[(wielkoscX-1)/2]+tablicaX[wielkoscX/2];
        medianaX=medianaX/2;
    }
    else
        medianaX=tablicaX[wielkoscX/2];
    return medianaX;
}
