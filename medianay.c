#include "f.h"

float medianaY(float *tablicaY, int wielkoscY);

float medianaY(float *tablicaY, int wielkoscY)
{
    float medianaY=0.0;
    if (wielkoscY%2==0)
    {
        medianaY=tablicaY[(wielkoscY-1)/2]+tablicaY[wielkoscY/2];
        medianaY=medianaY/2;
    }
    else
        medianaY=tablicaY[wielkoscY/2];
    return medianaY;
}
