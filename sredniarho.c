#include "f.h"

float sredniaRHO(float *tablicaRHO, int wielkoscRHO);

float sredniaRHO(float *tablicaRHO, int wielkoscRHO)
{
    float sumaRHO=0.0;
    for (int i=0; i<wielkoscRHO; i++)
        sumaRHO+=tablicaRHO[i];
    return sumaRHO/wielkoscRHO;
}
