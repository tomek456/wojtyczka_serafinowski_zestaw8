#include "f.h"

float sredniaX(float *tablicaX, int wielkoscX);

float sredniaX(float *tablicaX, int wielkoscX)
{
    float sumaX=0.0;
    for (int i=0; i<wielkoscX; i++)
        sumaX+=tablicaX[i];
    return sumaX/wielkoscX;
}
