CC=gcc
CLAGS=-Wall
LIBS=-lm

main: main.o wypisywanielp.o wypisywaniex.o wypisywaniey.o wypisywanierho.o sredniax.o sredniay.o sredniarho.o odchyleniex.o odchyleniey.o odchylenierho.o sortowaniex.o sortowaniey.o sortowanierho.o medianax.o medianay.o medianarho.o
	$(CC) $(CFLAGS) -o main main.o wypisywanielp.o wypisywaniex.o wypisywaniey.o wypisywanierho.o sredniax.o sredniay.o sredniarho.o odchyleniex.o odchyleniey.o odchylenierho.o sortowaniex.o sortowaniey.o sortowanierho.o medianax.o medianay.o medianarho.o $(LIBS)

main.o: main.c f.h
	$(CC) $(CFLAGS) -c main.c

wypisywanielp.o: wypisywanielp.c
	$(CC) $(CFLAGS) -c wypisywanielp.c

wypisywaniex.o: wypisywaniex.c
	$(CC) $(CFLAGS) -c wypisywaniex.c

wypisywaniey.o: wypisywaniey.c
	$(CC) $(CFLAGS) -c wypisywaniey.c

wypisywanierho.o: wypisywanierho.c
	$(CC) $(CFLAGS) -c wypisywanierho.c

sredniax.o: sredniax.c
	$(CC) $(CFLAGS) -c sredniax.c

sredniay.o: sredniay.c
	$(CC) $(CFLAGS) -c sredniay.c

sredniarho.o: sredniarho.c
	$(CC) $(CFLAGS) -c sredniarho.c

odchyleniex.o: odchyleniex.c
	$(CC) $(CFLAGS) -c odchyleniex.c

odchyleniey.o: odchyleniey.c
	$(CC) $(CFLAGS) -c odchyleniey.c

odchylenierho.o: odchylenierho.c
	$(CC) $(CFLAGS) -c odchylenierho.c

sortowaniex.o: sortowaniex.c
	$(CC) $(CFLAGS) -c sortowaniex.c

sortowaniey.o: sortowaniey.c
	$(CC) $(CFLAGS) -c sortowaniey.c

sortowanierho.o: sortowanierho.c
	$(CC) $(CFLAGS) -c sortowanierho.c

medianax.o: medianax.c
	$(CC) $(CFLAGS) -c medianax.c

medianay.o: medianay.c
	$(CC) $(CFLAGS) -c medianay.c

medianarho.o: medianarho.c
	$(CC) $(CFLAGS) -c medianarho.c
