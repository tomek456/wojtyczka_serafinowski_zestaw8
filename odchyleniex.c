#include "f.h"

float odchylenieX(float *tablicaX, int wielkoscX);

float odchylenieX(float *tablicaX, int wielkoscX)
{
    float sumaX=0.0;
    for (int i=0; i<wielkoscX; i++)
        sumaX+=tablicaX[i];
    float sredniaX=sumaX/wielkoscX;
    float licznik=0.0;
    for (int i=0; i<wielkoscX; i++)
        licznik+=pow(tablicaX[i]-sredniaX, 2);
    float ulamek=licznik/50;
    return sqrt(ulamek);
}
