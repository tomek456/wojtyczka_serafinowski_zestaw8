#include "f.h"

float odchylenieRHO(float *tablicaRHO, int wielkoscRHO);

float odchylenieRHO(float *tablicaRHO, int wielkoscRHO)
{
    float sumaRHO=0.0;
    for (int i=0; i<wielkoscRHO; i++)
        sumaRHO+=tablicaRHO[i];
    float sredniaRHO=sumaRHO/wielkoscRHO;
    float licznik=0.0;
    for (int i=0; i<wielkoscRHO; i++)
        licznik+=pow(tablicaRHO[i]-sredniaRHO, 2);
    float ulamek=licznik/50;
    return sqrt(ulamek);
}
