#include "f.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <float.h>

int main()
{
    FILE *plik;
    plik=fopen("P0001_attr.rec", "r");
    int line=0;
    char input[512];
    fgets(input, 512, plik);
    line=line+1;
    int wielkosc=0;
    printf("Podaj ilosc wszystkich wartosci ktore chcesz wyswietlic (max 200): ");
    scanf("%d", &wielkosc);
    if (wielkosc>200)
    {
        printf("\nSprobuj jeszcze raz podajac wartosc maksymalnie rowna 200\n");
        exit(0);
    }

    int wielkoscLP=0;
    float *tablicaLP;
    tablicaLP=(float *)malloc(wielkoscLP*sizeof(float));
    int wielkoscX=0;
    float *tablicaX;
    tablicaX=(float *)malloc(wielkoscX*sizeof(float));
    int wielkoscY=0;
    float *tablicaY;
    tablicaY=(float *)malloc(wielkoscY*sizeof(float));
    int wielkoscRHO=0;
    float *tablicaRHO;
    tablicaRHO=(float *)malloc(wielkoscRHO*sizeof(float));
    printf("\n");

    for (int i=0; i<wielkosc; i++)
    {
        if (i%4==0)
        {
            fscanf(plik, "%f", &tablicaLP[wielkoscLP]);
            wielkoscLP++;
        }
        else if (i%4==1)
        {
            fscanf(plik, "%f", &tablicaX[wielkoscX]);
            wielkoscX++;
        }
        else if (i%4==2)
        {
            fscanf(plik, "%f", &tablicaY[wielkoscY]);
            wielkoscY++;
        }
        else
        {
            fscanf(plik, "%f", &tablicaRHO[wielkoscRHO]);
            wielkoscRHO++;
        }
    }

    printf("Tablica LP:\n");
    wypisywanieLP(tablicaLP, wielkoscLP);
    printf("\nTablica X:\n");
    wypisywanieX(tablicaX, wielkoscX);
    printf("\nTablica Y:\n");
    wypisywanieY(tablicaY, wielkoscY);
    printf("\nTablica RHO:\n");
    wypisywanieRHO(tablicaRHO, wielkoscRHO);

    printf("\nSrednia dla tablicy X wynosi: %f\n", sredniaX(tablicaX, wielkoscX));
    printf("Srednia dla tablicy Y wynosi: %f\n", sredniaY(tablicaY, wielkoscY));
    printf("Srednia dla tablicy RHO wynosi: %f\n", sredniaRHO(tablicaRHO, wielkoscRHO));

    printf("\nOdchylenie standardowe dla tablicy X wynosi: %f\n", odchylenieX(tablicaX, wielkoscX));
    printf("Odchylenie standardowe dla tablicy Y wynosi: %f\n", odchylenieY(tablicaY, wielkoscY));
    printf("Odchylenie standardowe dla tablicy RHO wynosi: %f\n", odchylenieRHO(tablicaRHO, wielkoscRHO));

    sortowanieX(tablicaX, wielkoscX);
    sortowanieY(tablicaY, wielkoscY);
    sortowanieRHO(tablicaRHO, wielkoscRHO);

    printf("Mediana dla tablicy X wynosi: %f\n", medianaX(tablicaX, wielkoscX));
    printf("Mediana dla tablicy Y wynosi: %f\n", medianaY(tablicaY, wielkoscY));
    printf("Mediana dla tablicy RHO wynosi: %f\n", medianaRHO(tablicaRHO, wielkoscRHO));

    float srednia_1 = sredniaX(tablicaX, wielkoscX);
    float srednia_2 = sredniaY(tablicaY, wielkoscY);
    float srednia_3 = sredniaRHO(tablicaRHO, wielkoscRHO);
    float odchylenie_1 = odchylenieX(tablicaX, wielkoscX);
    float odchylenie_2 = odchylenieY(tablicaY, wielkoscY);
    float odchylenie_3 = odchylenieRHO(tablicaRHO, wielkoscRHO);
    float mediana_1 = medianaX(tablicaX, wielkoscX);
    float mediana_2 = medianaY(tablicaY, wielkoscY);
    float mediana_3 = medianaRHO(tablicaRHO, wielkoscRHO);

    fclose(plik);

    plik=fopen("P0001_attr.rec", "a+");

    fprintf(plik, "\nSrednia tablicy X: %f\n", srednia_1);
    fprintf(plik, "Srednia tablicy Y: %f\n", srednia_2);
    fprintf(plik, "Srednia tablicy RHO: %f\n", srednia_3);
    fprintf(plik, "Odchylenie standardowe tablicy X: %f\n", odchylenie_1);
    fprintf(plik, "Odchylenie standardowe tablicy Y: %f\n", odchylenie_2);
    fprintf(plik, "Odchylenie standardowe tablicy RHO: %f\n", odchylenie_3);
    fprintf(plik, "Mediana tablicy X: %f\n", mediana_1);
    fprintf(plik, "Mediana tablicy Y: %f\n", mediana_2);
    fprintf(plik, "Mediana tablicy RHO: %f\n", mediana_3);

    fclose(plik);

    free(tablicaLP);
    free(tablicaX);
    free(tablicaY);
    free(tablicaRHO);

    return 0;
}
