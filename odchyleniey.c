#include "f.h"

float odchylenieY(float *tablicaY, int wielkoscY);

float odchylenieY(float *tablicaY, int wielkoscY)
{
    float sumaY=0.0;
    for (int i=0; i<wielkoscY; i++)
        sumaY+=tablicaY[i];
    float sredniaY=sumaY/wielkoscY;
    float licznik=0.0;
    for (int i=0; i<wielkoscY; i++)
        licznik+=pow(tablicaY[i]-sredniaY, 2);
    float ulamek=licznik/50;
    return sqrt(ulamek);
}
