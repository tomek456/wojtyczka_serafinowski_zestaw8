#include "f.h"

float sredniaY(float *tablicaY, int wielkoscY);

float sredniaY(float *tablicaY, int wielkoscY)
{
    float sumaY=0.0;
    for (int i=0; i<wielkoscY; i++)
        sumaY+=tablicaY[i];
    return sumaY/wielkoscY;
}
