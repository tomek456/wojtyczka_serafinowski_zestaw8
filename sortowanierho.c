#include "f.h"

void sortowanieRHO(float *tablicaRHO, int wielkoscRHO);

void sortowanieRHO(float *tablicaRHO, int wielkoscRHO)
{
    int i, j, k;
	for (i=0; i<wielkoscRHO-1; i++)
	{
		for (j=0; j<wielkoscRHO-1-i; j++)
		{
			if (tablicaRHO[j]>tablicaRHO[j+1])
			{
				k=tablicaRHO[j+1];
				tablicaRHO[j+1]=tablicaRHO[j];
				tablicaRHO[j]=k;
			}
		}
    }
    printf("Tablica RHO z posortowanymi liczbami: \n");
    wypisywanieRHO(tablicaRHO, wielkoscRHO);
    printf("\n");
    return;
}
