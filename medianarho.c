#include "f.h"

float medianaRHO(float *tablicaRHO, int wielkoscRHO);

float medianaRHO(float *tablicaRHO, int wielkoscRHO)
{
    float medianaRHO=0.0;
    if (wielkoscRHO%2==0)
    {
        medianaRHO=tablicaRHO[(wielkoscRHO-1)/2]+tablicaRHO[wielkoscRHO/2];
        medianaRHO=medianaRHO/2;
    }
    else
        medianaRHO=tablicaRHO[wielkoscRHO/2];
    return medianaRHO;
}
