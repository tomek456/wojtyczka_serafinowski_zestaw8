#include "f.h"

void sortowanieX(float *tablicaX, int wielkoscX);

void sortowanieX(float *tablicaX, int wielkoscX)
{
    int i, j, k;
	for (i=0; i<wielkoscX-1; i++)
	{
		for (j=0; j<wielkoscX-1-i; j++)
		{
			if (tablicaX[j]>tablicaX[j+1])
			{
				k=tablicaX[j+1];
				tablicaX[j+1]=tablicaX[j];
				tablicaX[j]=k;
			}
		}
    }
    printf("\nTablica X z posortowanymi liczbami: \n");
    wypisywanieX(tablicaX, wielkoscX);
    printf("\n");
    return;
}
